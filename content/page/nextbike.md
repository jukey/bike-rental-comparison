---
title: Nextbike
subtitle: One of the global players. In Nuremberg called Norisbike. In Berlin cooperating with Deezer. 
comments: true
---

# Nextbike

## Price

Berlin
- Different prices. Year pass from Potsdam not valid.
Potsdam
- € 1 € for a 30 minutes ride
- € 48 for a one year pass: Usage up to 30 minutes per ride 
- "Nachtschwärmer" Tarif / Night-Tarif

## Deposit

- none

## Payment

- [ ] Credit card
 - [ ] store payment details for easy recharge
- [ ] Paypal
- [ ] Direct debit
- [ ] ...

## Apps

- [x] [iOS](https://itunes.apple.com/de/app/nextbike/id504288371)
- [x] [Android](https://play.google.com/store/apps/details?id=de.nextbike)
- [x] [Windows](https://www.microsoft.com/de-de/store/p/nextbike/9wzdncrdbxk3):

## Location

- Cities:
  - Berlin
  - Potsdam
  - Many others

## System

- Berlin: Hybrid (Stations only planned)
- Potsdam: Station based

## Comfort

- Rental process: 0 (You will get a number for the lock in your app)
- Return process: -- (In case you forget to do so it takses until the 22nd hour after the initial rental until you get an SMS stating you are still renting the bike)
- Bike saddle height adjustment: ++ (easy)
- Number of Gears: 3
- Comfort for tall people: 0 (Ok)

## Support

- Channels:
  - [x] In App Messages to report problems, including photo upload
  - [x] Pjone calls

## Bikes

- Mobike ?
  - Red Symbol
  - 1 Gear
- Mobike Lite 3.0
  - Not to be confirmed confirmed (I never found a bike with Gears, independend from the Symbol color): White Symbol on map inside the app
  - 3 Gears

## Links

- Websiten
  - [Nextbike Potsdam](https://www.nextbike.de/de/potsdam/)
- Apps: [iOS](https://itunes.apple.com/de/app/nextbike/id504288371) / [Android](https://play.google.com/store/apps/details?id=de.nextbike) / [Windows](https://www.microsoft.com/de-de/store/p/nextbike/9wzdncrdbxk3):
