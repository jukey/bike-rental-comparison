---
title: Comparison
subtitle: Bike rental comparison page
comments: true
---


| Bike Rental Provider | Locations | System | Price | Deposit | Payment | Apps | Comfort | Support | Details | 
| -------------------- | --------- | ------ | ----- | ------- | ------- | ---- | ------- | ------- | ------- |
| Mobike               | |||||||||
| Nextbike             | Berlin, Potsdam, Many others |||||||||
| Call-A-Bike          ||||||||||
| Donkey Republik      ||||||||||
| Lidl-Bike            ||||||||||
| obike                ||||||||||
| Lime                 ||||||||||

