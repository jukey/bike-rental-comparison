---
title: Mobike
subtitle: Orange or silver bikes from china
comments: true
---

# Mobike

## Price

- € 1 € for a 20 minutes ride
- € 5.90 for a 30 days pass: Usage up to 2h per ride

## Deposit

- € 2

## Payment

- [x] Credit card
 - [x] store payment details for easy recharge
- [ ] Paypal
- [ ] Direct debit
- [ ] ...

## Apps

- [x] iOS:
- [x] Android:
- [ ] Other:

## Location

- Cities:
  - Berlin
  - others

## Comfort

- Rental process: +++ (very easy and seamless)
- Bike saddle height adjustment: ++ (very easy but only in a relatively small range so tall people are going to get problems)
- Number of Gears: 1
- Comfort for tall people: -- (Small bikes in general)

## Support

- Channels:
  - [x] In App Messages

## Bikes

- Mobike ?
  - Red Symbol
  - 1 Gear
- Mobike Lite 3.0
  - Not to be confirmed confirmed (I never found a bike with Gears, independend from the Symbol color): White Symbol on map inside the app
  - 3 Gears

## Links

- Website: https://mobike.com/de/
- Apps: [iOS](https://itunes.apple.com/app/id1044535426) / [Android](https://play.google.com/store/apps/details?id=com.mobike.mobikeapp)
