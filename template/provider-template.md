# Name of bike rental provider

## Price

- 

## Deposit

- 

## Payment

- [ ] Credit card
 - [ ] store payment details for easy recharge
- [ ] Paypal
- [ ] Direct debit
- [ ] ...

## Apps

- [ ] iOS:
- [ ] Android:
- [ ] Other:

## Location

- Cities:
  - others

## Comfort

- Rental process:
- Bike saddle height adjustment:
- Number of Gears:
- Comfort for tall people:

## Support

- Channels:
  - [ ] In App Messages
  - [ ] others

## Bikes

- Model 1

## Links

- Website: 
- Apps: [iOS]() / [Android]()
