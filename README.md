# Bike Rental Comparison Page

[![Build Status](https://gitlab.com/jukey/bike-rental-comparison/badges/master/build.svg)](https://gitlab.com/jukey/bike-rental-comparison/-/jobs)

------

[Take me to the page](https://jukey.gitlab.io/bike-rental-comparison)

This is an independent just for fun project without any relation to one of the bike rental providers listed.

Contributions welcome! Please create a Merge Request or describe you change using an issue.

## Planed Activities

- [x] Find a static page generator that fits
  - It seems [hugo](https://gitlab.com/pages/hugo) fits well. Easy theme switching, fast buildtime, plugins 


## Why gitlab and why using a static page generator

I would like to lower the barrier for contributors as much as possible without the need to host a CMS or Wiki myself.

------

# How to build and run this page:

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
